package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * AuthorService is a common interface for service layer, implementation of
 * which is using for operating AUTHOR table in database;
 */

public interface AuthorService {

	/**
	 * This method is using for creating new AUTHOR object in database.
	 * 
	 * @param author
	 *            AUTHOR object that contains all filled fields.
	 * @return If succeed - returns ID(Long value) in database of created
	 *         AUTHOR, otherwise 0;
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public Long add(Author author) throws ServiceException;

	/**
	 * This method is using for updating AUTHOR object in database.
	 * 
	 * @param author
	 *            AUTHOR object that contains all filled fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public void update(Author author) throws ServiceException;

	/**
	 * This method is using for loading the AUTHOR object(row) from database if
	 * it exists.
	 * 
	 * @param authorId
	 *            ID of searching AUTHOR.
	 * @return author object with a specified AUTHOR ID. If it doesn't exists,
	 *         returns not null AUTHOR object, with null fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public Author loadById(Long authorId) throws ServiceException;

	/**
	 * This method is using for loading all authors from table AUTHOR from
	 * database;
	 * 
	 * @return list of existing authors in database(table AUTHOR);
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public List<Author> loadAll() throws ServiceException;

	/**
	 * This method is using for loading all non expired authors(they'r expired
	 * date is bigger than current date(time) and they have no published news;
	 * 
	 * @return list of found authors(see description)
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public List<Author> loadAllNonExpired() throws ServiceException;

}
