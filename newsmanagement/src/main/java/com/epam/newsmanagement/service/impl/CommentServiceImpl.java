package com.epam.newsmanagement.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * AuthorServiceImpl is an implementation of
 * {@link com.epam.newsmanagement.service.CommentService} interface. At this
 * point this class are injected by
 * {@link com.epam.newsmanagement.dao.CommentDao} object via Spring. So at now
 * it have the same functionality. But also it can be expanded by another
 * functional methods. NOTE:in all implemented methods added checking for
 * passing null values;
 * 
 */

@Transactional(rollbackFor = ServiceException.class)
public class CommentServiceImpl implements CommentService {

	private static final Logger logger = LoggerFactory
			.getLogger(CommentServiceImpl.class);

	private CommentDao commentDao;

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.dao.CommentDao};
	 * 
	 * @param commentDao
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.dao.CommentDao}.
	 */

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.CommentDao#delete(Long)}
	 */

	@Override
	public void delete(Long commentId) throws ServiceException {
		if (commentId != null) {
			try {
				commentDao.delete(commentId);
			} catch (DaoException e) {
				logger.error("Can't delete comment: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.CommentDao#add(Comment)}
	 */

	@Override
	public Long add(Comment comment) throws ServiceException {
		Long commentId = new Long(0);
		if (comment != null) {
			try {
				commentId = commentDao.add(comment);
			} catch (DaoException e) {
				logger.error("Can't create comment:", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return commentId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.CommentDao#loadById(Long)}
	 */

	@Override
	public Comment loadById(Long commentId) throws ServiceException {
		Comment comment = new Comment();
		if (commentId != null) {
			try {
				comment = commentDao.loadById(commentId);
			} catch (DaoException e) {
				logger.error("Can't find comment:", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return comment;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.CommentDao#update(Comment)}
	 */
	@Override
	public void update(Comment comment) throws ServiceException {
		if (comment != null) {
			try {
				commentDao.update(comment);
			} catch (DaoException e) {
				logger.error("Can't update comment", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.CommentDao#deleteByNewsId(Long)}
	 */

	@Override
	public void deleteByNewsId(Long newsId) throws ServiceException {
		if (newsId != null) {
			try {
				commentDao.deleteByNewsId(newsId);
			} catch (DaoException e) {
				logger.error("Can't delete comment by newsId: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}
}
