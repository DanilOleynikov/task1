package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * This class is testing
 * {@link com.epam.newsmanagement.service.impl.CommentServiceImpl} class. Here
 * we are using Mockito framework and mocked objects. In our case we have mocked
 * object of {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl} that
 * then set to real object of
 * {@link com.epam.newsmanagement.service.impl.CommentServiceImpl}; NOTE: all
 * methods that are testing here are not calling during tests, they are calling
 * on mocked objects, so there are NO interactions with database during test
 * invocation. So we can only check if necessary method was called and what
 * value does it return.
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	private JdbcCommentDaoImpl commentDao;
	private CommentServiceImpl commentService;

	/**
	 * This method is executing before test methods. It create object of
	 * {@link com.epam.newsmanagement.service.impl.CommentServiceImpl} class,
	 * also as mocked object of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl} class.
	 * 
	 */

	@Before
	public void setUp() {
		commentDao = Mockito.mock(JdbcCommentDaoImpl.class);
		commentService = new CommentServiceImpl();
		commentService.setCommentDao(commentDao);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#loadById(Long)}
	 * invoked) and what value does it return.
	 */

	@Test
	public void testLoadById() throws ServiceException, DaoException {
		Long dummyId = new Long(1);
		Comment dummyComment = new Comment();
		Comment expected = new Comment();

		when(commentService.loadById(dummyId)).thenReturn(dummyComment);
		expected = commentService.loadById(dummyId);

		verify(commentDao, atLeastOnce()).loadById(dummyId);
		Assert.assertEquals(expected, dummyComment);

	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#add(Comment)}
	 * invoked) and what value does it return.
	 */

	@Test
	public void testAdd() throws ServiceException, DaoException {
		Long dummyId = new Long(1);
		Comment dummyComment = new Comment();
		when(commentService.add(dummyComment)).thenReturn(dummyId);

		Long actual = commentService.add(dummyComment);
		Assert.assertEquals(dummyId, actual);
		verify(commentDao, atLeastOnce()).add(dummyComment);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#update(Comment)}
	 * invoked).
	 */

	@Test
	public void testUpdate() throws ServiceException, DaoException {
		Comment dummyComment = new Comment();
		commentService.update(dummyComment);
		verify(commentDao, atLeastOnce()).update(dummyComment);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#delete(Long)}
	 * invoked).
	 */

	@Test
	public void testDelete() throws ServiceException, DaoException {
		Long dummyId = new Long(1);
		commentService.delete(dummyId);
		verify(commentDao, atLeastOnce()).delete(dummyId);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#deleteByNewsId(Long)}
	 * invoked).
	 */

	@Test
	public void testDeleteByNewsId() throws ServiceException, DaoException {

		Long dummyNewsId = new Long(1);
		commentService.deleteByNewsId(dummyNewsId);
		verify(commentDao, atLeastOnce()).deleteByNewsId(dummyNewsId);
	}
}
