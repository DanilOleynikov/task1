package com.epam.newsmanagement.dao.impl;

import java.sql.Timestamp;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * This class is testing functionality of methods of
 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl} class. Here we
 * are using DI for injecting object of class that inherited from
 * {@link com.epam.newsmanagement.dao.CommentDao} and DBunit for
 * loading/deleting data to/from test database through prepared data set.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/comment-dataset.xml")
@DatabaseTearDown(value = "classpath:com/epam/newsmanagement/dao/impl/comment-dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class JdbcCommentDaoImplTest {

	@Autowired
	private CommentDao commentDao;

	/**
	 * This method is testing the functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#add(Comment)}
	 * Firstly we are making {@link com.epam.newsmanagement.entity.Comment}
	 * object and add him into table, then we are getting created object by ID
	 * and asserting it with expected object. If they are equals by all fields,
	 * test will complete successfully
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testAdd() throws DaoException {

		Comment expectedComment = new Comment();
		Comment actualComment = new Comment();

		expectedComment.setCreationTime(new Timestamp(new Date().getTime()));
		expectedComment.setText("comment text");
		expectedComment.setNewsId(new Long(1));

		long commentId = commentDao.add(expectedComment);
		actualComment = commentDao.loadById(commentId);
		expectedComment.setId(commentId);

		assertComment(expectedComment, actualComment);
	}

	/**
	 * This method is using for testing
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#delete(Long)}
	 * method Firstly we are deleting
	 * {@link com.epam.newsmanagement.entity.Comment} object from table, and
	 * then trying to get him by ID from database. If all fields in loaded
	 * object is null then test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testDelete() throws DaoException {
		Comment actualComment = new Comment();
		commentDao.delete(new Long(1));
		actualComment = commentDao.loadById(new Long(1));

		assertNull(actualComment);

	}

	/**
	 * This method is using for testing functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#loadById(Long)}
	 * method in CommentDao. We are trying to load
	 * {@link com.epam.newsmanagement.entity.Comment} object from database, and
	 * then checking all fields for values. If all fields in loaded object is
	 * equal to expected fields then test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testLoadById() throws DaoException {
		Comment actualComment = new Comment();
		actualComment = commentDao.loadById(new Long(1));

		Assert.assertEquals("text of comment 1", actualComment.getText());
		Assert.assertEquals("2015-08-15 20:20:55.0", actualComment
				.getCreationTime().toString());
		Assert.assertEquals(new Long(1), actualComment.getId());
		Assert.assertEquals(new Long(1), actualComment.getNewsId());

	}

	/**
	 * This method is testing functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#update(Comment)}
	 * method. We are updating object in database by ID, and then getting this
	 * object by ID and checking if it equals by all all fields with expected
	 * object. If they are completely equals, then test will complete
	 * successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testUpdate() throws DaoException {

		Comment expectedComment = new Comment();
		Comment actualComment = new Comment();

		expectedComment.setCreationTime(new Timestamp(new Date().getTime()));
		expectedComment.setId(new Long(1));
		expectedComment.setText("hello");
		expectedComment.setNewsId(new Long(1));

		commentDao.update(expectedComment);
		actualComment = commentDao.loadById(new Long(1));
		assertComment(expectedComment, actualComment);

	}

	/**
	 * This method is testing
	 * {@link com.epam.newsmanagement.dao.impl.JdbcCommentDaoImpl#deleteByNewsId(Long)}
	 * .It have same logic as {@link #testDelete()} method, but here we are
	 * trying to delete Comment object by NewsId;
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testDeleteByNewsId() throws DaoException {

		commentDao.deleteByNewsId(new Long(1));
		Comment actualComment = commentDao.loadById(new Long(1));

		assertNull(actualComment);

	}

	/**
	 * This method is asserting two Comment objects for equality of each field.
	 * 
	 * @param expected
	 *            expected Comment object;
	 * @param actual
	 *            actual Comment object;
	 */
	private void assertComment(Comment expected, Comment actual) {

		Assert.assertEquals(expected.getText(), actual.getText());
		Assert.assertEquals(expected.getCreationTime(),
				actual.getCreationTime());
		Assert.assertEquals(expected.getId(), actual.getId());
		Assert.assertEquals(expected.getNewsId(), actual.getNewsId());

	}

	/**
	 * This method is asserting each Comment object fields for null.
	 * 
	 * @param actual
	 *            actual Comment object;
	 */
	private void assertNull(Comment actual) {
		Assert.assertNull(actual.getText());
		Assert.assertNull(actual.getCreationTime());
		Assert.assertNull(actual.getId());
		Assert.assertNull(actual.getNewsId());
	}

}
