package com.epam.newsmanagement.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * This class is testing functionality of methods of
 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl} class. Here we are
 * using DI for injecting object of class that inherited from
 * {@link com.epam.newsmanagement.dao.AuthorDao} and DBunit for loading/deleting
 * data to/from test database through prepared data set.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/author-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/author-dataset.xml" }, type = DatabaseOperation.DELETE)
public class JdbcAuthorDaoImplTest {

	@Autowired
	private AuthorDao authorDao;

	/**
	 * This method is testing is functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#loadById(Long)}
	 * method. Here we are loading {@link com.epam.newsmanagement.entity.Author}
	 * object from test table and then checking all fields for null. If object
	 * has all proper(not null) filled fields test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testLoadById() throws DaoException {
		Author actual = new Author();
		actual = authorDao.loadById(new Long(2));

		Assert.assertEquals("Pavel", actual.getName());
		Assert.assertEquals(new Long(2), actual.getId());
		Assert.assertEquals("2015-08-15 20:20:55.0", actual.getExpiredTime()
				.toString());

	}

	/**
	 * This method is testing is functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#add(Author)} .
	 * Here we are creating {@link com.epam.newsmanagement.entity.Author} object
	 * and then trying to add him into test table and then loading created
	 * author by Id and checking retrieve author with created. If the have same
	 * fields test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testAdd() throws DaoException {
		Author expectedAuthor = new Author();
		Author actualAuthor = new Author();

		expectedAuthor.setExpiredTime(new Date(new Date().getTime()));
		expectedAuthor.setName("test author");

		long authorId = authorDao.add(expectedAuthor);
		actualAuthor = authorDao.loadById(authorId);
		expectedAuthor.setId(authorId);

		assertAuthor(expectedAuthor, actualAuthor);
	}

	/**
	 * This method is using for testing functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#update(Author)}
	 * . Firstly we are updating existing object in table, then we are
	 * retrieving it from table and check for equals by each fields. If both
	 * objects have same fields test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testUpdate() throws DaoException {
		Author expectedAuthor = new Author();
		Author actualAuthor = new Author();

		expectedAuthor.setExpiredTime(new Timestamp(new Date().getTime()));
		expectedAuthor.setName("New author name");
		expectedAuthor.setId(new Long(1));

		authorDao.update(expectedAuthor);
		actualAuthor = authorDao.loadById(new Long(1));

		assertAuthor(expectedAuthor, actualAuthor);
	}

	/**
	 * This method is using for testing
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#loadAll()} As
	 * we know the number of Author objects in dataset, we can just invoke this
	 * method and compare with static expected value. If we will get expected
	 * size of news list test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testLoadAll() throws DaoException {

		List<Author> actualAuthorList = new ArrayList<Author>();
		actualAuthorList = authorDao.loadAll();
		int expectedAuthorListSize = 3;
		Assert.assertEquals(expectedAuthorListSize, actualAuthorList.size());
	}

	/**
	 * This method is testing the functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcAuthorDaoImpl#loadAllNonExpired()}
	 * If we got expected size of list of authors with satisfactory
	 * parameters(expired>now and have no published news) test will complete
	 * successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testLoadAllNonExpired() throws DaoException {

		List<Author> actualAuthorList = new ArrayList<Author>();
		actualAuthorList = authorDao.loadAllNonExpired();
		Assert.assertEquals(1, actualAuthorList.size());

	}

	/**
	 * This method is asserting two Author objects for equality by each field.
	 * 
	 * @param expected
	 *            expected author object.
	 * @param actual
	 *            actual author object.
	 */
	private void assertAuthor(Author expected, Author actual) {
		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getExpiredTime(), actual.getExpiredTime());
		Assert.assertEquals(expected.getId(), actual.getId());
	}

}
