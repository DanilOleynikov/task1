package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.util.DatabaseUtil;
import com.epam.newsmanagement.dao.util.QueryBuilder;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * JdbcNewsDaoImpl class is a specific realization of
 * {@link com.epam.newsmanagement.dao.NewsDao} interface. According to this
 * class you can interact with database through simple JDBC.
 */
public class JdbcNewsDaoImpl implements NewsDao {

	private static final String SQL_CREATE_NEWS = "INSERT INTO news VALUES(news_seq.NEXTVAL,?,?,?,?,?)";
	private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE news_id = ?";
	private static final String SQL_UPDATE_NEWS = "UPDATE news SET title = ?, short_text = ?, full_text = ?, creation_date = ?, modification_date = ? WHERE news_id = ?";
	private static final String SQL_FIND_NEWS = "SELECT * FROM news WHERE news_id = ?";

	private static final String SQL_LOAD_LIST_OF_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE = "SELECT NEWS.NEWS_ID,NEWS.CREATION_DATE,NEWS.FULL_TEXT,NEWS.MODIFICATION_DATE,NEWS.SHORT_TEXT,NEWS.TITLE,COUNT(COMMENTS.COMMENT_ID) as comCounter FROM NEWS FULL OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID GROUP BY NEWS.NEWS_ID,NEWS.CREATION_DATE,NEWS.FULL_TEXT,NEWS.MODIFICATION_DATE,NEWS.SHORT_TEXT,NEWS.TITLE ORDER BY comCounter DESC, NEWS.MODIFICATION_DATE DESC";
	private static final String SQL_LOAD_SINGLE_NEWS_MESSAGE = "SELECT  NEWS.NEWS_ID,NEWS.TITLE,NEWS.SHORT_TEXT,NEWS.FULL_TEXT,NEWS.CREATION_DATE,NEWS.MODIFICATION_DATE, COMMENTS.COMMENT_TEXT,COMMENTS.CREATION_DATE as COMMENT_CREATION_DATE,COMMENTS.COMMENT_ID, TAG.TAG_NAME,TAG.TAG_ID, AUTHOR.AUTHOR_ID,AUTHOR.AUTHOR_NAME,AUTHOR.EXPIRED FROM NEWS_TAG INNER JOIN NEWS  ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID  INNER JOIN TAG  ON TAG.TAG_ID = NEWS_TAG.TAG_ID  INNER JOIN NEWS_AUTHOR  ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID  INNER JOIN AUTHOR  ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID  INNER JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID WHERE NEWS.NEWS_ID = ? GROUP BY NEWS.NEWS_ID,NEWS.TITLE,NEWS.SHORT_TEXT,NEWS.FULL_TEXT,NEWS.CREATION_DATE,NEWS.MODIFICATION_DATE, COMMENTS.COMMENT_TEXT,COMMENTS.CREATION_DATE,COMMENTS.COMMENT_ID,COMMENTS.NEWS_ID, TAG.TAG_NAME,TAG.TAG_ID,AUTHOR.AUTHOR_ID,AUTHOR.AUTHOR_NAME,AUTHOR.EXPIRED";

	private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO news_author VALUES(?,?)";
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "DELETE FROM news_author WHERE news_id=?";
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID = "DELETE FROM news_author WHERE author_id=?";

	private static final String SQL_ADD_NEWS_TAG = "INSERT INTO news_tag VALUES(?,?)";
	private static final String SQL_DELETE_NEWS_TAG_BY_TAG_ID = "DELETE FROM news_tag WHERE tag_id=?";
	private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "DELETE FROM news_tag WHERE news_id=?";

	private DataSource dataSource;

	/**
	 * This method is using for injecting(DI) datasource via Spring.
	 * 
	 * @param dataSource
	 *            Some datasource that already configured for concrete
	 *            database(e.g for oracle url/pass/login/driver).In this
	 *            application this setter is using for spring DI of DataSource.
	 *            Configuration for oracle database you can see in
	 *            database-config.properties file.
	 */

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#add(News)}
	 * method through simple JDBC.
	 */

	@Override
	public Long add(News news) throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long newsId = new Long(0);
		String[] pk = { "NEWS_ID" };

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_CREATE_NEWS, pk);
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			pst.setDate(5, new Date(news.getModificationDate().getTime()));
			pst.executeUpdate();

			rs = pst.getGeneratedKeys();

			if (rs != null && rs.next()) {
				newsId = rs.getLong(1);
			}
		}

		catch (SQLException ex) {
			throw new DaoException(ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return newsId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#update(News)} method through
	 * simple JDBC.
	 */

	@Override
	public void update(News news) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_UPDATE_NEWS);
			pst.setString(1, news.getTitle());
			pst.setString(2, news.getShortText());
			pst.setString(3, news.getFullText());
			pst.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			pst.setDate(5, new Date(news.getModificationDate().getTime()));
			pst.setLong(6, news.getId());
			pst.executeUpdate();
		}

		catch (SQLException ex) {

			throw new DaoException(ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#delete(Long)} method through
	 * simple JDBC.
	 */

	@Override
	public void delete(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		}

		catch (SQLException ex) {
			throw new DaoException(ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadById(Long)} method through
	 * simple JDBC.
	 */

	@Override
	public News loadById(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		News news = new News();

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_FIND_NEWS);
			pst.setLong(1, newsId);
			rs = pst.executeQuery();

			while (rs.next()) {

				news.setId(rs.getLong("news_id"));
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));

			}
		} catch (SQLException ex) {
			throw new DaoException(ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return news;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsVo(Long)} method
	 * through simple JDBC.
	 */

	@Override
	public NewsVO loadNewsVo(Long newsId) throws DaoException {
		NewsVO newsVo = new NewsVO();
		Author author = new Author();
		News news = new News();
		Set<Comment> comments = new HashSet<Comment>();
		Set<Tag> tags = new HashSet<Tag>();

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_LOAD_SINGLE_NEWS_MESSAGE,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

			pst.setLong(1, newsId);
			rs = pst.executeQuery();
			if (rs != null && rs.next()) {

				author.setExpiredTime(rs.getTimestamp("expired"));
				author.setId(rs.getLong("AUTHOR_ID"));
				author.setName(rs.getString("AUTHOR_NAME"));
				newsVo.setAuthor(author);

				news.setCreationDate(rs.getTimestamp("creation_date"));
				news.setFullText(rs.getString("full_text"));
				news.setId(rs.getLong("news_id"));
				news.setModificationDate(rs.getDate("modification_date"));
				news.setShortText(rs.getString("short_text"));
				news.setTitle(rs.getString("title"));
				newsVo.setNews(news);

			}
			comments = makeListOfCommentsFromResultSet(rs);
			tags = makeListOfTagsFromResultSet(rs);
			newsVo.setComments(comments);
			newsVo.setTags(tags);

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return newsVo;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.NewsDao#loadAll()}
	 * method through simple JDBC.
	 */

	@Override
	public List<News> loadAll() throws DaoException {

		List<News> sortedNews = new LinkedList<News>();
		Statement st = null;
		Connection cn = null;
		ResultSet rs = null;
		News news = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			st = cn.createStatement();
			rs = st.executeQuery(SQL_LOAD_LIST_OF_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE);
			if (rs != null) {
				while (rs.next()) {

					news = new News();
					news.setId(rs.getLong("NEWS_ID"));
					news.setTitle(rs.getString("TITLE"));
					news.setShortText(rs.getString("SHORT_TEXT"));
					news.setFullText(rs.getString("FULL_TEXT"));
					news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
					news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
					sortedNews.add(news);
				}
			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, st, rs,
					dataSource);
		}

		return sortedNews;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#findByCriteria(SearchCriteria)}
	 * method through simple JDBC.
	 */

	@Override
	public List<News> findByCriteria(SearchCriteria sc) throws DaoException {
		Connection cn = null;
		Statement st = null;
		ResultSet rs = null;
		List<News> criteriaNews = new LinkedList<News>();

		String criteriaQuery = QueryBuilder.buildQueryFromSearchCriteria(sc);
		if (criteriaQuery.isEmpty()) {
			criteriaNews = loadAll();
		}

		else {

			try {
				cn = DataSourceUtils.doGetConnection(dataSource);
				st = cn.createStatement();
				rs = st.executeQuery(criteriaQuery);
				if (rs != null) {
					while (rs.next()) {
						News foundNews = new News();
						foundNews.setCreationDate(rs
								.getTimestamp("creation_date"));
						foundNews.setFullText(rs.getString("full_text"));
						foundNews.setId(rs.getLong("news_id"));
						foundNews.setModificationDate(rs
								.getDate("modification_date"));
						foundNews.setShortText(rs.getString("short_text"));
						foundNews.setTitle(rs.getString("title"));
						criteriaNews.add(foundNews);
					}

				}
			} catch (SQLException ex) {
				throw new DaoException(ex.getMessage(), ex);
			} finally {
				DatabaseUtil.closeConnectionStatementResultSet(cn, st, rs,
						dataSource);
			}
		}

		return criteriaNews;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsAuthorLink(Long, Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void addNewsAuthorLink(Long newsId, Long authorId)
			throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_ADD_NEWS_AUTHOR);
			pst.setLong(1, newsId);
			pst.setLong(2, authorId);
			pst.execute();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long...)
	 * )} method through simple JDBC.
	 */

	public void addNewsTagLink(Long newsId, Long... tagId) throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_ADD_NEWS_TAG);

			for (Long tId : tagId) {
				pst.setLong(1, newsId);
				pst.setLong(2, tId);
				pst.addBatch();
			}
			pst.executeBatch();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void addNewsTagLink(Long newsId, Long tagId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_ADD_NEWS_TAG);
			pst.setLong(1, newsId);
			pst.setLong(2, tagId);
			pst.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByNewsId(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsAuthorLinkByNewsId(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByAuthorId(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsAuthorLinkByAuthorId(Long authorId)
			throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID);
			pst.setLong(1, authorId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLinkByNewsId(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsTagLinkByNewsId(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLink(Long)}
	 * method through simple JDBC.
	 */

	@Override
	public void deleteNewsTagLink(Long tagId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_NEWS_TAG_BY_TAG_ID);
			pst.setLong(1, tagId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * This method is using for retrieving data(Comment objects) from ResultSet.
	 * 
	 * @param rs
	 *            ResultSet object with necessary data(records of comments)
	 * @return Set of comments extracted from ResultSet.
	 * @throws DaoException
	 *             if any SQLException acquired on this layer
	 */
	private Set<Comment> makeListOfCommentsFromResultSet(ResultSet rs)
			throws DaoException {
		Set<Comment> comments = new HashSet<Comment>();
		try {
			rs.beforeFirst();

			while (rs.next()) {
				Comment comment = new Comment();
				comment.setCreationTime(rs
						.getTimestamp("comment_creation_date"));
				comment.setId(rs.getLong("comment_id"));
				comment.setNewsId(rs.getLong("news_id"));
				comment.setText(rs.getString("comment_text"));
				comments.add(comment);
			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return comments;
	}

	/**
	 * This method is using for retrieving data(Tag objects) from ResultSet.
	 * 
	 * @param rs
	 *            ResultSet object with necessary data(records of tags)
	 * @return Set of tags extracted from ResultSet.
	 * @throws DaoException
	 *             if any SQLException acquired on this layer
	 */
	private Set<Tag> makeListOfTagsFromResultSet(ResultSet rs)
			throws DaoException {
		Set<Tag> tags = new HashSet<Tag>();
		try {

			rs.beforeFirst();

			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getLong("tag_id"));
				tag.setName(rs.getString("tag_name"));
				tags.add(tag);
			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}
		return tags;
	}
}
