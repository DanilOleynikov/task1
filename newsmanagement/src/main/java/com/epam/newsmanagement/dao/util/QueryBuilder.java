package com.epam.newsmanagement.dao.util;

import com.epam.newsmanagement.entity.SearchCriteria;

/**
 * QueryBuilder class is using for creating different queries for database that
 * are using in DAO layer(methods)
 * 
 */
public class QueryBuilder {

	private static final String COMMA = ",";
	private static final String RIGHT_BRACKET = ")";
	private static final String AND_PART = " AND ";
	private static final String SQL_TAG_CLAUSE = " TAG.TAG_ID IN(";
	private static final String HAVING_CLAUSE = " HAVING COUNT(DISTINCT TAG.TAG_ID) = ";
	private static final String GROUP_CLAUSE = " GROUP BY NEWS.NEWS_ID,NEWS.TITLE,NEWS.SHORT_TEXT,NEWS.FULL_TEXT,NEWS.CREATION_DATE,NEWS.MODIFICATION_DATE";
	private static final String SQL_MAIN_PART = "SELECT NEWS.NEWS_ID,NEWS.TITLE,NEWS.SHORT_TEXT,NEWS.FULL_TEXT,NEWS.CREATION_DATE,NEWS.MODIFICATION_DATE FROM NEWS_TAG"
			+ " INNER JOIN NEWS ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID INNER JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID"
			+ " INNER JOIN NEWS_AUTHOR ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID INNER JOIN AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID"
			+ " WHERE";
	private static final String SQL_AUTHOR_CLAUSE = " AUTHOR.AUTHOR_ID=";

	/**
	 * This method is using for creating query from SerachCriteria object, that
	 * must contain searching authorId and array of tag id's.If one of the
	 * constituents is missing, it will build query for searching by parameter
	 * that is available. If both fields is SearchCriteria are missing it will
	 * return empty String;
	 * 
	 * @param searchCriteria
	 *            SearchCriteria object that contain searching authorId and list
	 *            of tag id's.
	 * @return proper string query if two or one of the fields in SearchCriteria
	 *         is not null, and empty - if both fields are empty.
	 */
	public static String buildQueryFromSearchCriteria(
			SearchCriteria searchCriteria) {

		StringBuilder resultQuery = new StringBuilder("");

		if (searchCriteria != null) {
			if (searchCriteria.getAuthorId() != null
					|| searchCriteria.getTagId() != null) {
				resultQuery.append(SQL_MAIN_PART);
				if (searchCriteria.getAuthorId() != null) {
					resultQuery.append(SQL_AUTHOR_CLAUSE);
					resultQuery.append(searchCriteria.getAuthorId());

				}

				if (searchCriteria.getTagId() != null) {
					if (searchCriteria.getAuthorId() != null) {
						resultQuery.append(AND_PART);
					}
					resultQuery.append(SQL_TAG_CLAUSE);
					for (Long i : searchCriteria.getTagId()) {
						resultQuery.append(i).append(COMMA);

					}
					resultQuery.deleteCharAt(resultQuery.length() - 1);
					resultQuery.append(RIGHT_BRACKET);
					resultQuery.append(HAVING_CLAUSE).append(
							searchCriteria.getTagId().length);
				}
				resultQuery.append(GROUP_CLAUSE);

			}
		}

		return resultQuery.toString();
	}

}
