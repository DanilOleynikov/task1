package com.epam.newsmanagement.entity;

import java.util.Arrays;

/**
 * SearchCriteria class is using for creating objects that consist of authorId
 * and list of id's of tags. This objects are using for search functionality in
 * DAO layer.
 */
public class SearchCriteria {

	private Long authorId;
	private Long[] tagId;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long[] getTagId() {
		return tagId;
	}

	public void setTagId(Long[] tagId) {
		this.tagId = tagId;
	}

	/**
	 * This method is using for string representation of SearchCriteria object.
	 */
	@Override
	public String toString() {
		return new StringBuilder("SearchCriteria [authorId=").append(authorId)
				.append(", tagId=").append(Arrays.toString(tagId)).append("]")
				.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + Arrays.hashCode(tagId);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchCriteria other = (SearchCriteria) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (!Arrays.equals(tagId, other.tagId))
			return false;
		return true;
	}

}
