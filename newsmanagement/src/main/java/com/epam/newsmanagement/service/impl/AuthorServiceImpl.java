package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * AuthorServiceImpl is an implementation of
 * {@link com.epam.newsmanagement.service.AuthorService} interface. At this
 * point this class are injected by
 * {@link com.epam.newsmanagement.dao.AuthorDao} object via Spring. So at now it
 * have the same functionality. But also it can be expanded by another
 * functional methods. NOTE:in all implemented methods added checking for
 * passing null values;
 * 
 */
@Transactional(rollbackFor = ServiceException.class)
public class AuthorServiceImpl implements AuthorService {

	private static final Logger logger = LoggerFactory
			.getLogger(AuthorServiceImpl.class);

	private AuthorDao authorDao;

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.dao.AuthorDao};
	 * 
	 * @param authorDao
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.dao.AuthorDao}.
	 */
	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.AuthorDao#add(Author)}
	 */

	@Override
	public Long add(Author author) throws ServiceException {

		Long authorId = new Long(0);

		if (author != null) {
			try {
				authorId = authorDao.add(author);
			} catch (DaoException e) {
				logger.error("Can't create author: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return authorId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.AuthorDao#update(Author)}
	 */

	@Override
	public void update(Author author) throws ServiceException {

		if (author != null) {
			try {
				authorDao.update(author);
			} catch (DaoException ex) {
				logger.error("Can't update author: ", ex);
				throw new ServiceException(ex.getMessage(), ex);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadById(Long)}
	 */

	@Override
	public Author loadById(Long authorId) throws ServiceException {
		Author author = new Author();
		if (authorId != null) {
			try {
				author = authorDao.loadById(authorId);
			} catch (DaoException e) {
				logger.error("Can't find author: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return author;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadAll()}
	 */

	@Override
	public List<Author> loadAll() throws ServiceException {

		List<Author> authorList = new ArrayList<Author>();
		try {
			authorList = authorDao.loadAll();
		} catch (DaoException e) {
			logger.error("Can't load all authors from database:", e);
			throw new ServiceException(e.getMessage(), e);

		}
		return authorList;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadAllNonExpired()}
	 */

	@Override
	public List<Author> loadAllNonExpired() throws ServiceException {
		List<Author> authorList = new ArrayList<Author>();

		try {
			authorList = authorDao.loadAllNonExpired();
		} catch (DaoException e) {
			logger.error("Can't load non expired authors:", e);
			throw new ServiceException(e.getMessage(), e);
		}
		return authorList;
	}

}
