--------------------------------------------------------
--  DROPING TABLES
--------------------------------------------------------
--------------------------------------------------------
--DROP TABLE NEWS_AUTHOR
--------------------------------------------------------
DROP TABLE NEWS_AUTHOR;
--------------------------------------------------------
--DROP TABLE NEWS_TAG
--------------------------------------------------------
DROP TABLE NEWS_TAG;
--------------------------------------------------------
--DROP TABLE COMMENTS
--------------------------------------------------------
DROP TABLE COMMENTS;
--------------------------------------------------------
--DROP TABLE TAG
--------------------------------------------------------
DROP TABLE TAG;
--------------------------------------------------------
--DROP TABLE AUTHOR
--------------------------------------------------------
DROP TABLE AUTHOR;
--------------------------------------------------------
--DROP TABLE NEWS
--------------------------------------------------------
DROP TABLE NEWS;
--------------------------------------------------------
--DROP TABLE ROLES
--------------------------------------------------------
DROP TABLE ROLES;
--------------------------------------------------------
--DROP TABLE USERS
--------------------------------------------------------
DROP TABLE USERS;
--------------------------------------------------------
--------------DROPING/CREATING SEQUENCES----------------
--------------------------------------------------------
--------------------------------------------------------
--CREATE NEWS_SEQ
--------------------------------------------------------
DROP SEQUENCE NEWS_SEQ;
CREATE SEQUENCE NEWS_SEQ START WITH 1 MINVALUE 0;
--------------------------------------------------------
--CREATE COMMENTS_SEQ
--------------------------------------------------------
DROP SEQUENCE COMMENTS_SEQ;
CREATE SEQUENCE COMMENTS_SEQ START WITH 1 MINVALUE 0;
--------------------------------------------------------
--CREATE AUTHOR_SEQ
--------------------------------------------------------
DROP SEQUENCE AUTHOR_SEQ;
CREATE SEQUENCE AUTHOR_SEQ START WITH 1 MINVALUE 0;
--------------------------------------------------------
--CREATE TAG_SEQ
--------------------------------------------------------
DROP SEQUENCE TAG_SEQ;
CREATE SEQUENCE TAG_SEQ START WITH 1 MINVALUE 0;
--------------------------------------------------------
--CREATE USERS_SEQ
--------------------------------------------------------
DROP SEQUENCE USERS_SEQ;
CREATE SEQUENCE USERS_SEQ  START WITH 1 MINVALUE 0;

--------------------------------------------------------
--------------CREATING TABLES---------------------------
--------------------------------------------------------
--------------------------------------------------------
CREATE TABLE NEWS(
NEWS_ID number(20) NOT NULL PRIMARY KEY,
TITLE nvarchar2(30) NOT NULL,
SHORT_TEXT nvarchar2(100) NOT NULL,
FULL_TEXT nvarchar2(2000) NOT NULL,
CREATION_DATE TIMESTAMP NOT NULL,
MODIFICATION_DATE DATE NOT NULL
);

CREATE TABLE COMMENTS(
COMMENT_ID number(20) NOT NULL PRIMARY KEY,
NEWS_ID number (20) NOT NULL,
COMMENT_TEXT nvarchar2(100) NOT NULL,
CREATION_DATE TIMESTAMP NOT NULL,
CONSTRAINT comments_news_fk FOREIGN KEY (NEWS_ID) REFERENCES NEWS(NEWS_ID)
);

CREATE TABLE AUTHOR(
AUTHOR_ID number(20) NOT NULL PRIMARY KEY,
AUTHOR_NAME nvarchar2(30) NOT NULL,
EXPIRED TIMESTAMP
);

CREATE TABLE NEWS_AUTHOR(
NEWS_ID number(20) NOT NULL,
AUTHOR_ID number(20) NOT NULL,
CONSTRAINT news_author_author_fk FOREIGN KEY (AUTHOR_ID) REFERENCES AUTHOR(AUTHOR_ID),
CONSTRAINT news_author_news_fk FOREIGN KEY (NEWS_ID) REFERENCES NEWS(NEWS_ID)
);

CREATE TABLE TAG(
TAG_ID number(20) NOT NULL PRIMARY KEY,
TAG_NAME nvarchar2(30) NOT NULL
);

CREATE TABLE NEWS_TAG(
NEWS_ID number(20) NOT NULL,
TAG_ID NUMBER(20) NOT NULL,
CONSTRAINT news_tag_news_fk FOREIGN KEY (NEWS_ID) REFERENCES NEWS(NEWS_ID),
CONSTRAINT news_tag_tag_fk FOREIGN KEY (TAG_ID) REFERENCES TAG(TAG_ID)
);

CREATE TABLE USERS(
USER_ID number(20) NOT NULL PRIMARY KEY,
USER_NAME nvarchar2(50) NOT NULL,
LOGIN varchar2(30) NOT NULL,
PASSWORD varchar2(30) NOT NULL
);

CREATE TABLE ROLES(
USER_ID number(20) NOT NULL,
ROLE_NAME varchar2(50) NOT NULL,
CONSTRAINT roles_users_fk FOREIGN KEY (USER_ID) REFERENCES USERS(USER_ID)
);
