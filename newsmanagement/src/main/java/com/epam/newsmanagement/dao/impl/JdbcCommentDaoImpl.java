package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.util.DatabaseUtil;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

/**
 * JdbcCommentDaoImpl class is a specific realization of
 * {@link com.epam.newsmanagement.dao.CommentDao} interface. According to this
 * class you can interact with database through simple JDBC.
 */

public class JdbcCommentDaoImpl implements CommentDao {
	private static final String SQL_CREATE_COMMENT = "INSERT INTO comments VALUES(comments_seq.NEXTVAl,?,?,?)";
	private static final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE comment_id=?";
	private static final String SQL_UPDATE_COMMENT = "UPDATE comments SET comment_text=?, creation_date=? WHERE comment_id=?";
	private static final String SQL_FIND_COMMENT = "SELECT * FROM comments WHERE comment_id=?";

	private static final String SQL_DELETE_BY_NEWS_ID = "DELETE FROM comments WHERE news_id=?";

	private DataSource dataSource;

	/**
	 * This method is using for injecting(DI) datasource via Spring.
	 * 
	 * @param dataSource
	 *            Some datasource that already configured for concrete
	 *            database(e.g for oracle url/pass/login/driver).In this
	 *            application this setter is using for spring DI of DataSource.
	 *            Configuration for oracle database you can see in
	 *            database-config.properties file.
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#add(Comment)} method
	 * through simple JDBC.
	 */

	public Long add(Comment comment) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String[] pk = { "COMMENT_ID" };
		Long commentId = new Long(0);

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_CREATE_COMMENT, pk);
			pst.setLong(1, comment.getNewsId());
			pst.setString(2, comment.getText());
			pst.setTimestamp(3, new Timestamp(comment.getCreationTime()
					.getTime()));
			pst.executeUpdate();

			rs = pst.getGeneratedKeys();

			if (rs != null && rs.next()) {
				commentId = rs.getLong(1);
			}

		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return commentId;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#delete(Long)} method
	 * through simple JDBC.
	 */

	public void delete(Long id) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_COMMENT);
			pst.setLong(1, id);
			pst.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#update(Comment)} method
	 * through simple JDBC.
	 */

	public void update(Comment comment) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_UPDATE_COMMENT);
			pst.setString(1, comment.getText());
			pst.setTimestamp(2, new Timestamp(comment.getCreationTime()
					.getTime()));
			pst.setLong(3, comment.getId());
			pst.executeUpdate();
		}

		catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#loadById(Long)} method
	 * through simple JDBC.
	 */

	public Comment loadById(Long id) throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Comment comment = new Comment();

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_FIND_COMMENT);

			pst.setLong(1, id);
			rs = pst.executeQuery();

			while (rs.next()) {
				comment.setId(rs.getLong("comment_id"));
				comment.setNewsId(rs.getLong("news_id"));
				comment.setText(rs.getString("comment_text"));
				comment.setCreationTime(rs.getTimestamp("creation_date"));
			}
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return comment;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.CommentDao#deleteByNewsId(Long)}
	 * method through simple JDBC.
	 */
	@Override
	public void deleteByNewsId(Long newsId) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_BY_NEWS_ID);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}
}
