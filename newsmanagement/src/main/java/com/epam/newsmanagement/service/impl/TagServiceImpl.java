package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

/**
 * TagServiceImpl is an implementation of
 * {@link com.epam.newsmanagement.service.TagService} interface. At this point
 * this class are injected by {@link com.epam.newsmanagement.dao.TagDao} and
 * {@link com.epam.newsmanagement.dao.NewsDao} objects via Spring. So at now it
 * have the same functionality. But also it can be expanded by another
 * functional methods. NOTE:in all implemented methods added checking for
 * passing null values;
 * 
 */

@Transactional(rollbackFor = ServiceException.class)
public class TagServiceImpl implements TagService {

	private static final Logger logger = LoggerFactory
			.getLogger(TagServiceImpl.class);

	private TagDao tagDao;
	private NewsDao newsDao;

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.dao.TagDao};
	 * 
	 * @param tagDao
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.dao.TagDao}.
	 */
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.dao.NewsDao};
	 * 
	 * @param newsDao
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.dao.NewsDao}.
	 */
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.TagDao#update(Tag)}
	 */

	@Override
	public void update(Tag tag) throws ServiceException {
		if (tag != null) {
			try {
				tagDao.update(tag);
			} catch (DaoException e) {
				logger.error("Can't update tag:", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.TagDao#delete(Long)} But also, before
	 * deleting from table, it delete any linking records between NEWS and TAG.
	 */
	@Override
	public void delete(Long tagId) throws ServiceException {
		if (tagId != null) {
			try {
				newsDao.deleteNewsTagLink(tagId);
				tagDao.delete(tagId);
			} catch (DaoException e) {
				logger.error("Can't delete tag:", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.TagDao#add(Tag)}
	 */
	@Override
	public Long add(Tag tag) throws ServiceException {
		Long tagId = new Long(0);
		if (tag != null) {
			try {
				tagId = tagDao.add(tag);
			} catch (DaoException e) {
				logger.error("Cant' create tag:", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return tagId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.TagDao#loadById(Long)}
	 */
	@Override
	public Tag loadById(Long tagId) throws ServiceException {
		Tag tag = new Tag();
		if (tagId != null) {
			try {
				tag = tagDao.loadById(tagId);
			} catch (DaoException e) {
				logger.error("Can't find tag:", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}

		return tag;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.TagDao#loadAll()}
	 */
	@Override
	public List<Tag> loadAll() throws ServiceException {
		List<Tag> tagList = new ArrayList<Tag>();
		try {
			tagList = tagDao.loadAll();
		} catch (DaoException e) {
			logger.error("Can't load all tags from database:", e);
			throw new ServiceException(e.getMessage(), e);
		}

		return tagList;
	}
}
