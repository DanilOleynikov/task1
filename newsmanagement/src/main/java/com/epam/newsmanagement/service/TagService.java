package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * TagService is a common interface for service layer, implementation of which
 * is using for operating TAG table in database;
 */

public interface TagService {

	/**
	 * This method is using for creating new TAG object in database.
	 * 
	 * @param tag
	 *            TAG object that contains all filled fields.
	 * @return If succeed - returns ID(Long value) in database of created TAG,
	 *         otherwise 0;
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public Long add(Tag tag) throws ServiceException;

	/**
	 * This method is using for updating TAG object in database.
	 * 
	 * @param tag
	 *            TAG object that contains all filled fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void update(Tag tag) throws ServiceException;

	/**
	 * This method is using for deleting TAG object(row) from database by ID of
	 * TAG.
	 * 
	 * @param tagId
	 *            ID of TAG that are going to be deleted.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void delete(Long tagId) throws ServiceException;

	/**
	 * This method is using for loading the TAG object(row) from database if it
	 * exists.
	 * 
	 * @param tagId
	 *            ID of searching TAG.
	 * @return tag object with a specified TAG ID. If it doesn't exists, returns
	 *         not null TAG object, with null fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public Tag loadById(Long tagId) throws ServiceException;

	/**
	 * This method is using for loading all tags from table TAG from database;
	 * 
	 * @return list of existing tags in database;
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public List<Tag> loadAll() throws ServiceException;

}
