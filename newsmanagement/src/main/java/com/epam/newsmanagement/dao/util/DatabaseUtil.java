package com.epam.newsmanagement.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * This class is using to provide user with such functionality as closing
 * resources that was opened according to get access to database, also as
 * converting different data from one state to another one.
 */
public class DatabaseUtil {

	/**
	 * This method is using for releasing resources such as connection and
	 * statement.
	 * 
	 * @param connection
	 *            connection object that is need to be released to pool.
	 * @param statement
	 *            statement object that is need to be closed.
	 * @param dataSource
	 *            datasource object with whom programm is working.
	 * @throws DaoException
	 *             DaoException arise if it occured.
	 */
	public static void closeConnectionStatement(Connection connection,
			Statement statement, DataSource dataSource) throws DaoException {

		if (connection != null) {
			try {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method is using for releasing resources such as connection,
	 * statement and resultset;
	 * 
	 * @param connection
	 *            connection object that is need to be released to pool.
	 * @param statement
	 *            statement object that is need to be closed.
	 * @param dataSource
	 *            datasource object with whom programm is working.
	 * @param resultSet
	 *            resultset object that is need to be closed;
	 * @throws DaoException
	 *             DaoException arise if it occured.
	 */

	public static void closeConnectionStatementResultSet(Connection connection,
			Statement statement, ResultSet resultSet, DataSource dataSource)
			throws DaoException {

		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}
		}

		if (connection != null) {
			try {
				DataSourceUtils.doReleaseConnection(connection, dataSource);
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}
		}

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new DaoException(e.getMessage(), e);
			}
		}

	}
	
	/**
	 * This method is using to get array that contain only all id's of  tag's that was passed into
	 * method.
	 * 
	 * @param tag
	 *            list of tag objects that are need to be converted.
	 * @return Long[] of tag id's that are converted from list of tag objects.
	 */
	public static Long[] convertTagIdFromListToArray(List<Tag> tag) {

		Long[] id = new Long[tag.size()];

		for (int i = 0; i < tag.size(); i++) {
			id[i] = tag.get(i).getId();
		}
		return id;
	}

	
}
