package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.util.DatabaseUtil;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * JdbcTagDaoImpl class is a specific realization of
 * {@link com.epam.newsmanagement.dao.TagDao} interface. According to this class
 * you can interact with database through simple JDBC.
 */
public class JdbcTagDaoImpl implements TagDao {

	private static final String SQL_CREATE_TAG = "INSERT INTO tag VALUES(tag_seq.NEXTVAL,?)";
	private static final String SQL_DELETE_TAG = "DELETE FROM tag WHERE tag_id = ?";
	private static final String SQL_UPDATE_TAG = "UPDATE tag SET tag_name = ? WHERE tag_id = ?";
	private static final String SQL_FIND_TAG = "SELECT * FROM tag WHERE tag_id = ?";
	private static final String SQL_LOAD_ALL = "SELECT * FROM TAG";

	private DataSource dataSource;

	/**
	 * This method is using for injecting(DI) datasource via Spring.
	 * 
	 * @param dataSource
	 *            Some datasource that already configured for concrete
	 *            database(e.g for oracle url/pass/login/driver).In this
	 *            application this setter is using for spring DI of DataSource.
	 *            Configuration for oracle database you can see in
	 *            database-config.properties file.
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.TagDao#loadById(Long)} method through
	 * simple JDBC.
	 */

	public Tag loadById(Long id) throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Tag tag = new Tag();

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_FIND_TAG);
			pst.setLong(1, id);
			rs = pst.executeQuery();

			while (rs.next()) {
				tag.setId(rs.getLong("tag_id"));
				tag.setName(rs.getString("tag_name"));
			}
		} catch (SQLException ex) {

			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return tag;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#add(Tag)}
	 * method through simple JDBC.
	 */

	public Long add(Tag tag) throws DaoException {

		PreparedStatement pst = null;
		Connection cn = null;
		ResultSet rs = null;
		String[] pk = { "TAG_ID" };
		long tagId = 0;
		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_CREATE_TAG, pk);
			pst.setString(1, tag.getName());
			pst.executeUpdate();

			rs = pst.getGeneratedKeys();

			if (rs != null && rs.next()) {
				tagId = rs.getLong(1);
			}

		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		} finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

		return tagId;
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#delete(Long)}
	 * method through simple JDBC.
	 */

	public void delete(Long id) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_DELETE_TAG);
			pst.setLong(1, id);
			pst.executeUpdate();
		}

		catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#update(Tag)}
	 * method through simple JDBC.
	 */

	public void update(Tag tag) throws DaoException {
		Connection cn = null;
		PreparedStatement pst = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_UPDATE_TAG);
			pst.setString(1, tag.getName());
			pst.setLong(2, tag.getId());
			pst.executeUpdate();
		}

		catch (SQLException ex) {
			throw new DaoException(ex.getMessage(), ex);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}

	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.TagDao#loadAll()}
	 * method through simple JDBC.
	 */
	@Override
	public List<Tag> loadAll() throws DaoException {
		List<Tag> tagList = new LinkedList<Tag>();

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_LOAD_ALL);
			rs = pst.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					Tag tag = new Tag();
					tag.setName(rs.getString("TAG_NAME"));
					tag.setId(rs.getLong("TAG_ID"));
					tagList.add(tag);
				}
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return tagList;
	}
}
