package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.util.DatabaseUtil;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

/**
 * JdbcAuthorDaoImpl class is a specific realization of
 * {@link com.epam.newsmanagement.dao.AuthorDao} interface. According to this
 * class you can interact with database through simple JDBC. Also it's using
 * injection of dataSource through Spring.
 */
public class JdbcAuthorDaoImpl implements AuthorDao {

	private static final String SQL_CREATE_AUTHOR = "INSERT INTO author VALUES(author_seq.NEXTVAL,?,?)";
	private static final String SQL_UPDATE_AUTHOR = "UPDATE author SET author_name = ?, expired = ? WHERE author_id = ?";
	private static final String SQL_FIND_AUTHOR = "SELECT * FROM author WHERE author_id = ?";
	private static final String SQL_LOAD_ALL = "SELECT * FROM AUTHOR";
	private static final String SQL_LOAD_ALL_NON_EXPIRED = "SELECT * FROM author WHERE EXPIRED>SYSTIMESTAMP AND AUTHOR_ID NOT IN(SELECT AUTHOR_ID FROM NEWS_AUTHOR)";

	private DataSource dataSource;

	/**
	 * This method is using for injecting(DI) datasource via Spring.
	 * 
	 * @param dataSource
	 *            Some datasource that already configured for concrete
	 *            database(e.g for oracle/mysql url/pass/login/driver).In this
	 *            application this setter is using for spring DI of DataSource.
	 *            Configuration for oracle database you can see in
	 *            database-config.properties file.
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#add(Author)} method through
	 * simple JDBC.
	 */
	public Long add(Author author) throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Long authorId = new Long(0);
		String[] pk = { "AUTHOR_ID" };

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_CREATE_AUTHOR, pk);
			pst.setString(1, author.getName());
			pst.setTimestamp(2,
					new Timestamp(author.getExpiredTime().getTime()));
			pst.executeUpdate();

			rs = pst.getGeneratedKeys();
			if (rs != null && rs.next()) {

				authorId = rs.getLong(1);
			}

		} catch (SQLException e) {

			throw new DaoException(e.getMessage(), e);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return authorId;

	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#update(Author)} method
	 * through simple JDBC.
	 */
	public void update(Author author) throws DaoException {

		PreparedStatement pst = null;
		Connection cn = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_UPDATE_AUTHOR);
			pst.setString(1, author.getName());
			pst.setTimestamp(2,
					new Timestamp(author.getExpiredTime().getTime()));
			pst.setLong(3, author.getId());
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		finally {
			DatabaseUtil.closeConnectionStatement(cn, pst, dataSource);
		}
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadById(Long)} method
	 * through simple JDBC.
	 */
	public Author loadById(Long id) throws DaoException {
		PreparedStatement pst = null;
		Connection cn = null;
		ResultSet rs = null;
		Author author = new Author();

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_FIND_AUTHOR);
			pst.setLong(1, id);

			rs = pst.executeQuery();

			while (rs.next()) {
				author.setId(rs.getLong("author_id"));
				author.setName(rs.getString("author_name"));
				author.setExpiredTime(rs.getTimestamp("expired"));
			}

		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		}

		finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return author;

	}

	/**
	 * Implementation of {@link com.epam.newsmanagement.dao.AuthorDao#loadAll()}
	 * method through simple JDBC.
	 */
	@Override
	public List<Author> loadAll() throws DaoException {
		List<Author> authorList = new LinkedList<Author>();

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_LOAD_ALL);
			rs = pst.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					Author author = new Author();
					author.setExpiredTime(rs.getTimestamp("EXPIRED"));
					author.setId(rs.getLong("AUTHOR_ID"));
					author.setName("AUTHOR_NAME");
					authorList.add(author);

				}
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}

		return authorList;
	}

	/**
	 * Implementation of
	 * {@link com.epam.newsmanagement.dao.AuthorDao#loadAllNonExpired()} method
	 * through simple JDBC.
	 */
	@Override
	public List<Author> loadAllNonExpired() throws DaoException {

		Connection cn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Author> authorList = new LinkedList<Author>();

		try {
			cn = DataSourceUtils.doGetConnection(dataSource);
			pst = cn.prepareStatement(SQL_LOAD_ALL_NON_EXPIRED);
			rs = pst.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					Author author = new Author();
					author.setExpiredTime(rs.getTimestamp("EXPIRED"));
					author.setId(rs.getLong("AUTHOR_ID"));
					author.setName(rs.getString("AUTHOR_NAME"));
					authorList.add(author);
				}
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage(), e);
		} finally {
			DatabaseUtil.closeConnectionStatementResultSet(cn, pst, rs,
					dataSource);
		}
		return authorList;
	}
}
