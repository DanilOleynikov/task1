package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

/**
 * AuthorDao is a common interface for interactions with table AUTHOR in
 * database.In order to interact with this table, implement this interface and
 * write you realization for particular database and type of interaction(e.g.
 * JDBC,Hibernate);
 */
public interface AuthorDao {

	/**
	 * This method is using for creating new AUTHOR object in database.
	 * 
	 * @param author
	 *            AUTHOR object that contains all filled fields.
	 * @return If succeed - returns ID(Long value) in database of created
	 *         AUTHOR, otherwise 0;
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public Long add(Author author) throws DaoException;

	/**
	 * This method is using for updating AUTHOR object in database.
	 * 
	 * @param author
	 *            AUTHOR object that contains all filled fields.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public void update(Author author) throws DaoException;

	/**
	 * This method is using for loading the AUTHOR object(row) from database if
	 * it exists.
	 * 
	 * @param authorId
	 *            ID of searching AUTHOR.
	 * @return author object with a specified AUTHOR ID. If it doesn't exists,
	 *         returns not null AUTHOR object, with null fields.
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public Author loadById(Long authorId) throws DaoException;

	/**
	 * This method is using for loading all authors from table AUTHOR from
	 * database;
	 * 
	 * @return list of existing authors in database(table AUTHOR);
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public List<Author> loadAll() throws DaoException;

	/**
	 * This method is using for loading all non expired authors(their expired
	 * date is bigger than current date(time) and they have no published news;
	 * 
	 * @return list of found authors(see description of method)
	 * @throws DaoException
	 *             this exception throws when on DAO layer (e.g.) SQLException
	 *             caught.
	 */
	public List<Author> loadAllNonExpired() throws DaoException;

}
