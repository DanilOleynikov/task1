package com.epam.newsmanagement.dao.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * This class is testing functionality of methods of
 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl} class. Here we are
 * using DI for injecting object of class that inherited from
 * {@link com.epam.newsmanagement.dao.NewsDao} and DBunit for loading/deleting
 * data to/from test database through prepared data set.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/news-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/news-dataset.xml" }, type = DatabaseOperation.DELETE_ALL)
public class JdbcNewsDaoImplTest {

	@Autowired
	private NewsDao newsDao;

	/**
	 * This method is testing is functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#add(News)} . Here
	 * we are creating object of {@link com.epam.newsmanagement.entity.News} and
	 * then trying to add it into test table and then loading created news by ID
	 * and checking retrieve news with created. If the have same fields test
	 * will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testAdd() throws DaoException, ParseException {

		News actualNews = new News();
		News expectedNews = buildNews();
		long newsId = newsDao.add(expectedNews);
		expectedNews.setId(newsId);
		actualNews = newsDao.loadById(newsId);
		assertNews(expectedNews, actualNews);
	}

	/**
	 * This method is using for testing of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#delete(Long)}
	 * method. Firstly we are deleting News object from table, and then trying
	 * to get it by ID from database. If all fields in loaded object is null
	 * then test is completed.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testDelete() throws DaoException {

		News actualNews = new News();
		News expectedNews = buildNews();
		long newsId = newsDao.add(expectedNews);
		newsDao.delete(newsId);
		actualNews = newsDao.loadById(newsId);

		Assert.assertNull(actualNews.getFullText());
		Assert.assertNull(actualNews.getShortText());
		Assert.assertNull(actualNews.getTitle());
		Assert.assertNull(actualNews.getCreationDate());
		Assert.assertNull(actualNews.getId());
		Assert.assertNull(actualNews.getModificationDate());
	}

	/**
	 * This method is testing functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#update(News)}
	 * method. We are updating object in database, and then getting this object
	 * by ID and checking if they are equals by each field. If two objects equal
	 * by all fields test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testUpdate() throws DaoException, ParseException {

		News actualNews = new News();
		News expectedNews = buildNews();
		newsDao.update(expectedNews);
		actualNews = newsDao.loadById(new Long(1));
		assertNews(expectedNews, actualNews);

	}

	/**
	 * This method is using for testing functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#loadById(Long)}
	 * method. We are trying to load News object from database, and then
	 * checking all fields for values. If they are correct the test is
	 * completed.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testLoadById() throws DaoException, ParseException {

		News actualNews = new News();
		News expectedNews = buildNews();
		long newsId = newsDao.add(expectedNews);
		expectedNews.setId(newsId);
		actualNews = newsDao.loadById(newsId);
		assertNews(expectedNews, actualNews);
	}

	/**
	 * This method is using for testing
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#loadAll()}
	 * method. As we know the number of News objects in data set table, we can
	 * just invoke this method and compare with static expected value. If
	 * expected size of list of news the same as actual, so the test will
	 * complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testLoadAll() throws DaoException {
		List<News> actualNews = newsDao.loadAll();
		Assert.assertEquals(new Long(3), new Long(actualNews.size()));
	}

	/**
	 * This method is testing the functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#findByCriteria(SearchCriteria)}
	 * method. If after executing of method we have any object of News in list
	 * with previous populated fields the test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testFindByCriteria() throws DaoException {
		SearchCriteria sc = new SearchCriteria();
		sc.setAuthorId(new Long(1));
		sc.setTagId(new Long[] { new Long(1), new Long(2) });

		List<News> criteriaNews = newsDao.findByCriteria(sc);
		for (News news : criteriaNews) {
			assertNotNull(news);
		}
	}

	/**
	 * This method is using for testing functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl#loadNewsVo(Long)}
	 * method. If after executing we got VO with all not null fields the test
	 * will complete.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testLoadNewsVo() throws DaoException {
		NewsVO newsVo = newsDao.loadNewsVo(new Long(1));
		assertNotNullNewsVo(newsVo);

	}

	/**
	 * This method is checking NewsVO object fields for null.
	 * 
	 * @param newsVo
	 *            NewsVO object.
	 */
	private void assertNotNullNewsVo(NewsVO newsVo) {
		Assert.assertNotNull(newsVo.getAuthor());
		Assert.assertNotNull(newsVo.getComments());
		Assert.assertNotNull(newsVo.getNews());
		Assert.assertNotNull(newsVo.getTags());

	}

	/**
	 * This method is checking News object fields for null.
	 * 
	 * @param actual
	 *            News object.
	 */
	private void assertNotNull(News actual) {
		Assert.assertNotNull(actual.getFullText());
		Assert.assertNotNull(actual.getShortText());
		Assert.assertNotNull(actual.getTitle());
		Assert.assertNotNull(actual.getCreationDate());
		Assert.assertNotNull(actual.getId());
		Assert.assertNotNull(actual.getModificationDate());
	}

	/**
	 * This method is using for asserting two News object for fields equality.
	 * 
	 * @param expected
	 *            expected News object.
	 * @param actual
	 *            actual News object.
	 * @throws ParseException
	 *             arose if exception in during parsing of date acquired.
	 */
	private void assertNews(News expected, News actual) throws ParseException {
		Assert.assertEquals(expected.getFullText(), actual.getFullText());
		Assert.assertEquals(expected.getShortText(), actual.getShortText());
		Assert.assertEquals(expected.getTitle(), actual.getTitle());
		Assert.assertEquals(expected.getCreationDate(),
				actual.getCreationDate());
		Assert.assertEquals(expected.getId(), actual.getId());

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date formattedExpectedsNewsModificationDate = formatter.parse(formatter
				.format(expected.getModificationDate()));

		Assert.assertEquals(formattedExpectedsNewsModificationDate,
				actual.getModificationDate());
	}

	/**
	 * This method is creating news News object with previous populated fields.
	 * 
	 * @return News object.
	 */
	private News buildNews() {
		News news = new News();
		news.setCreationDate(new Timestamp(new Date().getTime()));
		news.setFullText("test full text");
		news.setModificationDate(new Date(new Date().getTime()));
		news.setShortText("test short text");
		news.setTitle("test title");
		news.setId(new Long(1));
		return news;
	}

}
