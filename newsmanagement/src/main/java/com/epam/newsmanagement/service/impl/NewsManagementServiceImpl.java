package com.epam.newsmanagement.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagmentService;
import com.epam.newsmanagement.service.NewsService;

/**
 * This class is using for fast manipulating over data in database.It consist of
 * four Service classes, that are providing this functionality. Also all class
 * marked as with annotation Transactional, due to all operations in provided
 * methods should be fully completed during invocation.
 * 
 */
@Transactional(rollbackFor = ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagmentService {

	private NewsService newsService;
	private CommentService commentService;

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.service.NewsService};
	 * 
	 * @param newsService
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.service.NewsService};
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.service.CommentService};
	 * 
	 * @param commentService
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.service.CommentService};
	 */
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	/**
	 * This method is using for creating new News record in database, with
	 * mentioned author and tags. Firstly it will create new News object, then
	 * it will make connections in connective tables between News and Author,
	 * and News and Tag; For more description look
	 * {@link com.epam.newsmanagement.service.NewsManagmentService#saveNewsWithAuthorAndTags(News, Long, Long...)}
	 */

	@Override
	public Long saveNewsWithAuthorAndTags(News news, Long authorId,
			Long... tagId) throws ServiceException {

		Long newsId = newsService.add(news);
		newsService.addNewsAuthorLink(news.getId(), authorId);
		newsService.addNewsTagLink(news.getId(), tagId);
		return newsId;
	}

	/**
	 * This method is using for deleting News object from database, with it all
	 * connections with tags and authors, also as comments. Firstly it will
	 * delete all connection in connective tables,then deleting of comments,and
	 * finally News object itself. For more description look
	 * {@link com.epam.newsmanagement.service.NewsManagmentService#deleteNews(Long)}
	 */
	@Override
	public void deleteNews(Long newsId) throws ServiceException {

		newsService.deleteNewsAuthorLinkByNewsId(newsId);
		commentService.deleteByNewsId(newsId);
		newsService.deleteNewsTagLinkByNewsId(newsId);
		newsService.delete(newsId);

	}
}
