package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * This class is testing functionality of methods of
 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl} class. Here we are
 * using DI for injecting object of class that inherited from
 * {@link com.epam.newsmanagement.dao.TagDao} and DBunit for loading/deleting
 * data to/from test database through prepared data set.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-application-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:com/epam/newsmanagement/dao/impl/tag-dataset.xml")
@DatabaseTearDown(value = { "classpath:com/epam/newsmanagement/dao/impl/tag-dataset.xml" }, type = DatabaseOperation.DELETE)
public class JdbcTagDaoImplTest {

	@Autowired
	private TagDao tagDao;

	/**
	 * This method is testing is functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#loadById(Long)} .
	 * Here we are loading {@link com.epam.newsmanagement.entity.Tag} object
	 * from test table and then checking all fields for null. If object has all
	 * proper filled fields test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testLoadById() throws DaoException {
		Tag tag = new Tag();
		tag = tagDao.loadById(new Long(1));
		Assert.assertEquals(new Long(1), tag.getId());
		Assert.assertEquals("yolo123", tag.getName());
	}

	/**
	 * This method is testing is functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#add(Tag)}Here we
	 * are creating {@link com.epam.newsmanagement.entity.Tag} object and then
	 * trying to add him into test table and then loading created tag by Id and
	 * checking retrieve tag with created. If the have same fields test will
	 * complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testAdd() throws DaoException {

		Tag expectedTag = new Tag();
		Tag actualTag = new Tag();
		expectedTag.setName("test tag");
		long tagId = tagDao.add(expectedTag);
		actualTag = tagDao.loadById(tagId);
		expectedTag.setId(tagId);
		assertTag(expectedTag, actualTag);

	}

	/**
	 * This method is testing is functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#delete(Long)}Here
	 * we are deleting {@link com.epam.newsmanagement.entity.Tag} object and
	 * then trying to get him from test table. If it doesnt exist test will
	 * complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */
	@Test
	public void testDelete() throws DaoException {
		Tag actualTag = new Tag();
		tagDao.delete(new Long(1));
		actualTag = tagDao.loadById(new Long(1));

		Assert.assertNull(actualTag.getName());
		Assert.assertNull(actualTag.getId());
	}

	/**
	 * This method is using for testing
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#loadAll()} As we
	 * know the number of Tag objects in dataset, we can just invoke this method
	 * and compare with static expected value. If expected size will the same as
	 * the size of got list, the test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testLoadAll() throws DaoException {

		List<Tag> actualTagList = new ArrayList<Tag>();
		actualTagList = tagDao.loadAll();
		int expectedTagListSize = 4;
		Assert.assertEquals(expectedTagListSize, actualTagList.size());
	}

	/**
	 * This method is using for testing functionality of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#update(Tag)}
	 * method in TagDao; Firstly we are updating existing object in table, then
	 * we are trying to get it from table and check for equals by each fields.
	 * If both objects have equal fields, the test will complete successfully.
	 * 
	 * @throws DaoException
	 *             if any SQLException arose during invocation.
	 */

	@Test
	public void testUpdate() throws DaoException {

		Tag expectedTag = new Tag();
		Tag actualTag = new Tag();
		expectedTag.setName("edited tag");
		expectedTag.setId(new Long(1));
		tagDao.update(expectedTag);
		actualTag = tagDao.loadById(new Long(1));

		assertTag(expectedTag, actualTag);
	}

	/**
	 * This method is using for asserting two tag objects for equality by
	 * fields.
	 * 
	 * @param expected
	 *            tag object.
	 * @param actual
	 *            tag object.
	 */
	private void assertTag(Tag expected, Tag actual) {
		Assert.assertEquals(expected.getName(), actual.getName());
		Assert.assertEquals(expected.getId(), actual.getId());
	}
}
