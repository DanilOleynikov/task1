package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * NewsService is a common interface for service layer, implementation of which
 * is using for operating NEWS table in database;
 */

public interface NewsService {

	/**
	 * This method is using for creating new NEWS object in database.
	 * 
	 * @param news
	 *            NEWS object that contains all filled fields.
	 * @return If succeed - returns ID(Long value) in database of created NEWS,
	 *         otherwise 0;
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public Long add(News news) throws ServiceException;

	/**
	 * This method is using for updating NEWS object in database.
	 * 
	 * @param news
	 *            NEWS object that contains all filled fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void update(News news) throws ServiceException;

	/**
	 * This method is using for deleting NEWS object(row) from database by ID of
	 * NEWS.
	 * 
	 * @param newsId
	 *            ID of NEWS that are going to be deleted.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void delete(Long newsId) throws ServiceException;

	/**
	 * This method is using for loading the NEWS object(row) from database if it
	 * exists.
	 * 
	 * @param newsId
	 *            ID of searching NEWS.
	 * @return News object with a specified NEWS ID. If it doesn't exists,
	 *         returns not null NEWS object, with null fields.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public News loadById(Long newsId) throws ServiceException;

	/**
	 * This method is using for loading NewsVO object by newsId. This object
	 * will contain set of tags and comments, also as author and news objects.
	 * 
	 * @param newsId
	 *            ID of searching news.
	 * @return NewsVO object that contain tags,comments,author,news;
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public NewsVO loadNewsVo(Long newsId) throws ServiceException;

	/**
	 * This method is using fore loading all news that are in database.ONE
	 * REMARK that list of news will be sorted by the number of comments and
	 * modification date.
	 * 
	 * @return list of news sorted by number of comments;
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public List<News> loadAll() throws ServiceException;

	/**
	 * This method is using for searching news by SearchCriteria. Full
	 * description of building query for searching you can find in
	 * {@link com.epam.newsmanagement.dao.util.QueryBuilder#buildQueryFromSearchCriteria(SearchCriteria)}
	 * method; NOTE: if
	 * {@link com.epam.newsmanagement.dao.util.QueryBuilder#buildQueryFromSearchCriteria(SearchCriteria)}
	 * method return empty String(cause of both parameters in SearchCriteria
	 * object is null or missing), result list of news will contain all news
	 * sorted by number of comments and modification date;
	 * 
	 * @param sc
	 *            {@link com.epam.newsmanagement.entity.SearchCriteria} object
	 *            that must contain all filled fields(for more info look method
	 *            description);
	 * @return list of news that satisfactory by passed parameters(for other
	 *         cases look method description)
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public List<News> findByCriteria(SearchCriteria sc) throws ServiceException;

	/**
	 * This method is using for creating new linking record in NEWS_AUTHOR
	 * table.
	 * 
	 * @param newsId
	 *            ID of NEWS object to connect.
	 * @param authorId
	 *            ID of AUTHOR object to connect.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public void addNewsAuthorLink(Long newsId, Long authorId)
			throws ServiceException;

	/**
	 * This method is using for creating new linking records in NEWS_TAG table.
	 * 
	 * @param newsId
	 *            ID of NEWS object to connect.
	 * @param tagId
	 *            ID of TAG's objects to connect.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void addNewsTagLink(Long newsId, Long... tagId)
			throws ServiceException;

	/**
	 * This method is using for creating new linking record in NEWS_TAG table.
	 * 
	 * @param newsId
	 *            ID of NEWS object to connect.
	 * @param tagId
	 *            ID of TAG object to connect.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void addNewsTagLink(Long newsId, Long tagId) throws ServiceException;

	/**
	 * This method is using for deleting linking record from NEWS_TAG table.
	 * 
	 * @param newsId
	 *            ID of NEWS object to delete from linking table.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public void deleteNewsTagLinkByNewsId(Long newsId) throws ServiceException;

	/**
	 * This method is using for deleting linking record from NEWS_TAG table.
	 * 
	 * @param tagId
	 *            ID of TAG object to delete from linking table.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */
	public void deleteNewsTagLink(Long tagId) throws ServiceException;

	/**
	 * This method is using for deleting linking record from NEWS_AUTHOR table.
	 * 
	 * @param newsId
	 *            ID of NEWS object to delete from linking table.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void deleteNewsAuthorLinkByNewsId(Long newsId)
			throws ServiceException;

	/**
	 * This method is using for deleting linking record from NEWS_AUTHOR table.
	 * 
	 * @param authorId
	 *            ID of AUTHOR object to delete from linking table.
	 * @throws ServiceException
	 *             this exception throws when on service layer (e.g.)
	 *             DaoException caught.
	 */

	public void deleteNewsAuthorLinkByAuthorId(Long authorId)
			throws ServiceException;

}
