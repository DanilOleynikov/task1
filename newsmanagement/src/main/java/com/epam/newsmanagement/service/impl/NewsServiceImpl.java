package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * NewsServiceImpl is an implementation of
 * {@link com.epam.newsmanagement.service.NewsService} interface. At this point
 * this class are injected by {@link com.epam.newsmanagement.dao.NewsDao} object
 * via Spring. So at now it have the same functionality. But also it can be
 * expanded by another functional methods. NOTE:in all implemented methods added
 * checking for passing null values;
 */

@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements NewsService {

	private static final Logger logger = LoggerFactory
			.getLogger(NewsServiceImpl.class);

	private NewsDao newsDao;

	/**
	 * This method is using for injecting(DI) object via Spring, that inherited
	 * from {@link com.epam.newsmanagement.dao.NewsDao};
	 * 
	 * @param newsDao
	 *            any class that inherited from
	 *            {@link com.epam.newsmanagement.dao.NewsDao}.
	 */
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#add(News)}
	 */

	@Override
	public Long add(News news) throws ServiceException {

		Long newsId = new Long(0);
		if (news != null) {
			try {
				newsId = newsDao.add(news);
			} catch (DaoException e) {
				logger.error("Can't create new news: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return newsId;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#update(News)}
	 */
	@Override
	public void update(News news) throws ServiceException {
		if (news != null) {
			try {
				newsDao.update(news);
			} catch (DaoException e) {
				logger.error("Can't update news: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#delete(Long)}
	 */
	@Override
	public void delete(Long newsId) throws ServiceException {
		if (newsId != null) {
			try {
				newsDao.delete(newsId);
			} catch (DaoException e) {
				logger.error("Can't delete news", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadById(Long)}
	 */
	@Override
	public News loadById(Long newsId) throws ServiceException {
		News news = new News();
		if (newsId != null) {
			try {
				news = newsDao.loadById(newsId);
			} catch (DaoException e) {
				logger.error("Can't load news by id: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
		return news;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadNewsVo(Long)}
	 */

	@Override
	public NewsVO loadNewsVo(Long newsId) throws ServiceException {
		NewsVO newsVo = new NewsVO();
		if (newsId != null) {
			try {
				newsVo = newsDao.loadNewsVo(newsId);
			} catch (DaoException e) {
				logger.error("Can't load newsVo by id: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}

		return newsVo;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#loadAll()}
	 */
	@Override
	public List<News> loadAll() throws ServiceException {
		List<News> news = null;
		try {
			news = newsDao.loadAll();
		} catch (DaoException e) {
			logger.error("Can't load all news: ", e);
			throw new ServiceException(e.getMessage(), e);
		}
		return news;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#findByCriteria(SearchCriteria)}
	 */
	@Override
	public List<News> findByCriteria(SearchCriteria sc) throws ServiceException {
		List<News> resultNews = new ArrayList<News>();
		try {
			resultNews = newsDao.findByCriteria(sc);
		} catch (DaoException e) {
			logger.error("Can't find news by SearchCriteria: ", e);
			throw new ServiceException(e.getMessage(), e);
		}
		return resultNews;
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsAuthorLink(Long, Long)}
	 */
	@Override
	public void addNewsAuthorLink(Long newsId, Long authorId)
			throws ServiceException {

		if (newsId != null && authorId != null) {
			try {
				newsDao.addNewsAuthorLink(newsId, authorId);
			} catch (DaoException e) {
				logger.error("Can't add newsAuthor: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long)}
	 */
	@Override
	public void addNewsTagLink(Long newsId, Long tagId) throws ServiceException {
		if (newsId != null && tagId != null) {
			try {
				newsDao.addNewsTagLink(newsId, tagId);
			} catch (DaoException e) {
				logger.error("Can't add news tag: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#addNewsTagLink(Long, Long...)}
	 */
	@Override
	public void addNewsTagLink(Long newsId, Long... tagId)
			throws ServiceException {

		if (newsId != null && tagId != null) {
			try {
				newsDao.addNewsTagLink(newsId, tagId);
			} catch (DaoException e) {
				logger.error("Can't add newsTag by variable number of tagId: ",
						e);
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByNewsId(Long)}
	 */
	@Override
	public void deleteNewsAuthorLinkByNewsId(Long newsId)
			throws ServiceException {

		if (newsId != null) {
			try {
				newsDao.deleteNewsAuthorLinkByNewsId(newsId);
			} catch (DaoException e) {
				logger.error("Can't delete newsAuthor by newsId: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsAuthorLinkByAuthorId(Long)}
	 */
	@Override
	public void deleteNewsAuthorLinkByAuthorId(Long authorId)
			throws ServiceException {
		if (authorId != null) {
			try {
				newsDao.deleteNewsAuthorLinkByAuthorId(authorId);
			} catch (DaoException e) {
				logger.error("Can't delete newsAuthor by authorId: ", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}

	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLinkByNewsId(Long)}
	 */
	@Override
	public void deleteNewsTagLinkByNewsId(Long newsId) throws ServiceException {

		if (newsId != null) {
			try {
				newsDao.deleteNewsTagLinkByNewsId(newsId);
			} catch (DaoException e) {
				logger.error("Can't delete newsTag by newsId:", e);
				throw new ServiceException(e.getMessage(), e);
			}
		}
	}

	/**
	 * This method has the same functionality as
	 * {@link com.epam.newsmanagement.dao.NewsDao#deleteNewsTagLink(Long)}
	 */
	@Override
	public void deleteNewsTagLink(Long tagId) throws ServiceException {
		if (tagId != null) {
			try {
				newsDao.deleteNewsTagLink(tagId);
			} catch (DaoException e) {
				logger.error("Can't delete newsTag by tagId: ", e);
				throw new ServiceException(e.getMessage(), e);
			}

		}

	}

}
