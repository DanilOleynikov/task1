package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl;
import com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * This class is testing
 * {@link com.epam.newsmanagement.service.impl.TagServiceImpl} class. Here we
 * are using Mockito framework and mocked objects. In our case we have mocked
 * objects of {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl},
 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl}, that then set to
 * real object of {@link com.epam.newsmanagement.service.impl.TagServiceImpl};
 * NOTE: all methods that are testing here are not calling during tests, they
 * are calling on mocked objects, so there are NO interactions with database
 * during test invocation. So we can only check if necessary method was called
 * and what value does it return.
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	private TagServiceImpl tagService;
	private JdbcTagDaoImpl tagDao;
	private JdbcNewsDaoImpl newsDao;

	/**
	 * This method is executing before test methods. It create object of
	 * {@link com.epam.newsmanagement.service.impl.TagServiceImpl} class, also
	 * as mocked objects of
	 * {@link com.epam.newsmanagement.dao.impl.JdbcNewsDaoImpl} and
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl} classes.
	 * 
	 */

	@Before
	public void setUp() {

		tagDao = Mockito.mock(JdbcTagDaoImpl.class);
		newsDao = Mockito.mock(JdbcNewsDaoImpl.class);
		tagService = new TagServiceImpl();
		tagService.setTagDao(tagDao);
		tagService.setNewsDao(newsDao);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#loadById(Long)}
	 * invoked) and what value does it return.
	 */

	@Test
	public void testLoadById() throws ServiceException, DaoException {

		Long tagId = new Long(1);
		tagService.loadById(new Long(tagId));
		verify(tagDao, atLeastOnce()).loadById(tagId);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#delete(Long)} and
	 * newsDao.deleteNewsTagLink() invoked).
	 */

	@Test
	public void testDelete() throws ServiceException, DaoException {

		Long tagId = new Long(1);
		tagService.delete(tagId);
		verify(tagDao, atLeastOnce()).delete(tagId);
		verify(newsDao, atLeastOnce()).deleteNewsTagLink(tagId);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#add(Tag)} invoked)
	 * and what value does it return.
	 */

	@Test
	public void testAdd() throws ServiceException, DaoException {

		Tag dummyTag = new Tag();
		Long dummyId = new Long(1);
		when(tagService.add(dummyTag)).thenReturn(dummyId);

		Long actual = tagService.add(dummyTag);
		Assert.assertEquals(dummyId, actual);
		verify(tagDao, atLeastOnce()).add(dummyTag);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#update(Tag)}
	 * invoked)
	 */

	@Test
	public void testUpdate() throws ServiceException, DaoException {

		Long dummyId = new Long(1);
		Tag dummyTag = new Tag();
		Tag expected = new Tag();

		when(tagService.loadById(dummyId)).thenReturn(dummyTag);
		expected = tagService.loadById(dummyId);

		verify(tagDao, atLeastOnce()).loadById(dummyId);
		Assert.assertEquals(expected, dummyTag);
	}

	/**
	 * This method is testing is the necessary method( in our case
	 * {@link com.epam.newsmanagement.dao.impl.JdbcTagDaoImpl#loadAll()}
	 * invoked) and what value does it return.
	 */

	@Test
	public void testLoadAll() throws ServiceException, DaoException {
		List<Tag> dummyTagList = new ArrayList<Tag>();
		List<Tag> expectedTagList = new ArrayList<Tag>();

		when(tagService.loadAll()).thenReturn(dummyTagList);
		expectedTagList = tagService.loadAll();

		verify(tagDao, atLeastOnce()).loadAll();
		Assert.assertEquals(expectedTagList, dummyTagList);
	}
}
